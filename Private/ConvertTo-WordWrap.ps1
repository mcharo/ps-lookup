function ConvertTo-WordWrap
{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [string]$InputString,
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        $Length
    )

    $Delimiters = @(' ', '-', "`n")
    ForEach($Paragraph in $InputString -Split("`n"))
    {
        $ParagraphWords = $Paragraph.Split(' ')
        $Wrapped = @('')
        $WrappedIndex = 0
        ForEach($Word in $ParagraphWords)
        {
            if (($Wrapped[$WrappedIndex].Length + $Word.Length) -gt $Length)
            {
                if ($Word -like '*-*' -and ($Wrapped[$WrappedIndex].Length + $Word.Split('-')[0].Length + 1) -le $Length)
                {
                    $SplitWord = $Word.Split('-')
                    $Wrapped[$WrappedIndex] += "$($SplitWord[0])-"
                    $Word = $SplitWord[1..$SplitWord.Length] -join '-'
                }
                $Wrapped[$WrappedIndex] = $Wrapped[$WrappedIndex].Trim()
                $Wrapped += ""
                $WrappedIndex++
            }
            $Wrapped[$WrappedIndex] += "$Word "
        }
        $Wrapped
        ''
    }
}