function Get-Definition
{
  param(
    [Parameter(Mandatory)]
    [string]$Word,
    [switch]$IncludeVariants
  )
  $out = Invoke-RestMethod -Method Get -Uri http://api.pearson.com/v2/dictionaries/ldoce5/entries?headword=$Word
  if ($out.results.senses)
  {


    if ($IncludeVariants)
    {
      $Results = $out.results
    }
    else
    {
      $Results = $out.results | Where-Object {$_.headword -eq $Word}
    }

    ForEach ($Result in $Results) {
      Write-Host "$($Result.headword):" -ForegroundColor Green
      $Count = 1
      ForEach ($Definition in $Result.senses.definition) {
        Write-Host "$Count. " -NoNewline -ForegroundColor Green
        ConvertTo-WordWrap -InputString $Definition -Length ($Host.UI.RawUI.BufferSize.Width - 4)
        $Count++
      }

    }
  }
}

New-Alias -Name def -Value Get-Definition