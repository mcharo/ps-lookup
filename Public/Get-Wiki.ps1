function Get-Wiki
{
  param(
    [string]$Title,
    [switch]$Open
  )
  $Title = [System.Net.WebUtility]::HtmlEncode($Title)
  $out = Invoke-RestMethod -Method Get -Uri "https://en.wikipedia.org/w/api.php?action=query&prop=info|extracts&inprop=url&exintro=&explaintext=&redirects=1&format=json&titles=$Title"
  $Page = $out.query.pages.psobject.Properties.value
  if ($Page.extract)
  {
    $Extract = $Page.extract.Trim()
    Write-Host $out.query.pages.psobject.Properties.value.title -ForegroundColor Green
    ConvertTo-WordWrap -InputString $Extract -Length ($Host.UI.RawUI.BufferSize.Width - 2)
    $Page.canonicalurl
    if ($Open)
    {
      Start-Process $Page.canonicalurl
    }
  }
}

New-Alias -Name wiki -Value Get-Wiki