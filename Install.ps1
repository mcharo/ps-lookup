function Install-LocalModule
{
    param(
        $ModuleName,
        $ModulePath=$PSScriptRoot
    )
    function Import-PSD
    {
        param(
            $Path
        )
        $ScriptAst = [System.Management.Automation.Language.Parser]::ParseFile($Path,[ref]$null,[ref]$null)
        $AstTables = $ScriptAst.FindAll({$args[0] -is [System.Management.Automation.Language.HashtableAst]}, $true)
        $AstTables[0].SafeGetValue()
    }

    Remove-Module $ModuleName -ErrorAction SilentlyContinue
    if ((Test-Path "$ModulePath\$ModuleName.psd1") -and (Test-Path "$ModulePath\$ModuleName.psm1"))
    {
        $ModuleFound = $true
        $ManifestVersion = [version](Import-PSD -Path "$ModulePath\$ModuleName.psd1").ModuleVersion
        $ModuleVersion = (Get-Module -ListAvailable $ModuleName).Version
        $NeedsUpdate = $ManifestVersion -gt $ModuleVersion
    }
    if ($ModuleFound)
    {
        if ($NeedsUpdate)
        {
            New-Item "$Env:UserProfile\Documents\WindowsPowerShell\Modules\$ModuleName" -ItemType Directory -ErrorAction SilentlyContinue | Out-Null
            Copy-Item "$ModulePath\$ModuleName.psd1" "$Env:UserProfile\Documents\WindowsPowerShell\Modules\$ModuleName\$ModuleName.psd1" -Force
            Copy-Item "$ModulePath\$ModuleName.psm1" "$Env:UserProfile\Documents\WindowsPowerShell\Modules\$ModuleName\$ModuleName.psm1" -Force
            Copy-Item "$ModulePath\Public\" "$Env:UserProfile\Documents\WindowsPowerShell\Modules\$ModuleName\" -Recurse -Force -ErrorAction SilentlyContinue
            Copy-Item "$ModulePath\Private\" "$Env:UserProfile\Documents\WindowsPowerShell\Modules\$ModuleName\" -Recurse -Force -ErrorAction SilentlyContinue
            try
            {
                Import-Module $ModuleName
                Write-Host "Successfully installed $ModuleName module."
            }
            catch
            {
                Import-Module "$ModulePath\$ModuleName.psm1"
            }
        }
        else # No update needed
        {
            Import-Module $ModuleName
        }

    }
    else
    {
        Write-Host "Unable to find $ModuleName module."
    }
}

Install-LocalModule -ModuleName PSLookup