## PSLookup
Get word definitions and Wikipedia summaries

### Requirements:
- PowerShell 4.0

### Usage
#### Setup
- Install the modules in your user modules path by running `.\Install.ps1`

#### Examples
- Define 'hilarious':
    - `Get-Definition hilarious`
    - `def hilarious`
    - `def liquid -IncludeVariants`

- Get Jaws Wikipedia summary:
    - `Get-Wiki 'Jaws (film)`
    - `wiki 'Jaws (film)'`